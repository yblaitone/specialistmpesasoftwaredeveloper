﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SpecialistMpesaSoftwareDeveloper.Models
{
    //classe modelo
    public class Model
    {
        [Required(ErrorMessage = "Por favor introduza o seu Nome.")]
        [DisplayName("Nome")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Por favor introduza o seu Apelido.")]
        [DisplayName("Apelido")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Por favor introduza a sua Data de Nasicmento.")]
        [DisplayName("Data de Nasicmento")]
        [DataType(DataType.Date)]
        [CustomBirthDate(ErrorMessage = "A sua idade é inferior a 16 anos.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Por favor introduza o seu Tipo de Documento de Identificação.")]
        [DisplayName("Tipo de Documento de Identificação")]
        public string IdType { get; set; }

        [Required(ErrorMessage = "Por favor introduza o seu Número do Documento de Identificação.")]
        [DisplayName("Número do Documento de Identificação")]
        public string IdNumber { get; set; }

        [DisplayName("Número de Telefone")]
        [Required(ErrorMessage = "Por favor introduza o seu Número de Telefone.")]
        [CustomCheckNumber(ErrorMessage = "O seu Número de Telefone não é da Operadora Vodacom, certifique-se que comece por 84/85.")]
        [MinLength(9, ErrorMessage = "O seu Número de Telefone deve conter 9 caracteres.")]
        [MaxLength(9, ErrorMessage = "O seu Número de Telefone deve conter 9 caracteres.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Certifique-se de introduzir apenas valores numéricos.")]
        public string PhoneNumber { get; set; }
    }
    

}