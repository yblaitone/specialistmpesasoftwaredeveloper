﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

//validadores customizados
namespace SpecialistMpesaSoftwareDeveloper.Models
{
    //valida se idade introduzida é maior que 16 anos
    public class CustomBirthDate : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int BirthDate = Convert.ToDateTime(value).Year;
            return DateTime.Now.Year - BirthDate > 16;
        }
    }

    //verifica se o número é da vodacom
    public class CustomCheckNumber : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;
            else
                return value.ToString()[1].Equals('4') || value.ToString()[1].Equals('5');
        }
    }

}