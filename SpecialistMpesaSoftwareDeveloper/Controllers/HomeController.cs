﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpecialistMpesaSoftwareDeveloper.Models;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace SpecialistMpesaSoftwareDeveloper.Controllers
{
    public class HomeController : Controller
    {
        //popula o dropdown de documentos
        string[] IdTypes = { "BI", "Passaporte", "Carta de Condução" };
        string Baseurl = "http://some.mock.api/create";

        public ActionResult Index()
        {
            ViewBag.CheckIdNr = false;
            ViewBag.Success = false;

            ViewBag.IdType = new SelectList(IdTypes);

            return View();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Index(Model Model)
        {
            ViewBag.CheckIdNr = false;
            ViewBag.Success = false;

            ViewBag.IdType = new SelectList(IdTypes);

            string IdType = Model.IdType;
            string IdNumber = Model.IdNumber;

            //faz a verificação do número de caracteres do documento dependendo do tipo
            if ((IdType == "BI" && IdNumber.Length != 13) || (IdType == "Passaporte" && IdNumber.Length != 9) || (IdType == "Carta de Condução" && IdNumber.Length != 10))
            {
                ViewBag.CheckIdNr = true;
                return View();
            }

            //verifica se todas validações foram cumpridas
            if (ModelState.IsValid)
            {
                using (var http = new HttpClient())
                {
                    //autenticação
                    http.DefaultRequestHeaders.Add("apiKey", "someRandomKey");

                    var data = new Model();

                    //conversão dos dados para JSON
                    var content = new StringContent(JsonConvert.SerializeObject(data));

                    //definição do tipo de dados que o request leva
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    var request = http.PostAsync(Baseurl, content);

                    var response = request.Result.Content.ReadAsStringAsync().Result;

                    return View();
                }

            }

            return View();

        }

    }

}